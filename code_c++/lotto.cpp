
#include <iostream>
//输入的字符是单个字符，不符合规则会进入死循环 

using namespace std;
long double probability(unsigned numbers, unsigned picks);

int main(void)
{
	double total, choices;
	cout << "enter the total number of choices on the game card and the number of picks allowed: \n";
	while ((cin >> total >> choices) && choices <= total)
	{
		cout << "you have one change in " << probability(total, choices);
		cout << " of winning. \n";
		cout << "Next two numbers (q to quit): ";
	}
	cout << "bye\n";
	
    return 0;
}

long double probability(unsigned numbers, unsigned picks)
{
	long double result = 1.0;
	long double n;
	unsigned p;
	
	for(n = numbers, p = picks; p > 0; n--, p--)
		result = result * n/p;
	return result;
}

