
#include <iostream>
#include <ctime>

const int ArSize = 20;

int main(void)
{
	using namespace std;

	char ch;
	int count = 0;
	cout << "enter characters; enter # to quit: \n";
	cin.get(ch);//读取输入中的下一个字符（即使它是空格） 
	
	while (cin.fail() == false)
	{
		cout << ch;
		++count;
		cin.get(ch);
	}
	cout << endl << count << " characters read!\n";

    return 0;
}


