
#include <iostream>

const int Cities = 5;
const int Years = 4;

int main(void)
{
	using namespace std;

	const char *cities[Cities] = 
	{
		"Gribble City",
		"Gribbletown",
		"New Gribble",
		"San Gribble",
		"Gribble Vista"
	};
	
	int maxtemps[Years][Cities] = 
	{
		{96,66,78,65,67},
		{96,100,45,63,36},
    	{78,65,47,89,72},
    	{98,95,68,83,27}
	};
	
	cout << "Maximum temperatures for 2008 - 2011 \n\n";
	for (int city = 0; city < Cities; ++city)
	{
		cout << cities[city] << ":\t";
		for (int year = 0; year < Years; ++year)
			cout << maxtemps[year][city] << "\t";
		cout << endl;
		
	 } 
	

    return 0;
}


