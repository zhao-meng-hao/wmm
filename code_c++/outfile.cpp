
#include <iostream>
#include <fstream>


int main(void)
{
	using namespace std;
	
	char automobile[50];
	int year;
	double a_price;
	double d_price;
	
	ofstream outFile;
	outFile.open("carinfo.txt");
	
	cout << "enter the make and mode1 of automobile: ";
	cin.getline(automobile, 50);
	cout << "enter the mode1 year: ";
	cin >> year;
	cout << "enter the original asking price: ";
	cin >> a_price;
	d_price = 0.913 * a_price;
	
	cout << fixed;
	cout.precision(2);
	cout.setf(ios_base::showpoint);
	cout << "make and mode1: " << automobile << endl;
	cout << "year: " << year << endl;
	cout << "was asking $ " << a_price << endl;
	cout << "now asking $ " << d_price << endl;
	
	outFile << fixed;
	outFile.precision(2);
	outFile.setf(ios_base::showpoint) ;
	outFile << "make and mode1: " << automobile << endl;
	outFile << "year: " << year << endl;
	outFile << "was asking $ " << a_price << endl;
	outFile << "now asking $ " << d_price << endl;
	
	outFile.close();
    return 0;
}


