
#include <iostream>
#include <ctime>

const int ArSize = 20;

int main(void)
{
	using namespace std;
	cout << "enter the delay time, in seconds: ";
	float secs;
	cin >> secs;
	clock_t delay = secs * CLOCKS_PER_SEC;
	cout << "starting\a\n";
	clock_t start = clock();
	while (clock() - start < delay);
	cout << "start : " << start << endl;
	cout << "CLOCKS_PER_SEC: " << CLOCKS_PER_SEC << endl;
	cout << "done \a\n";

    return 0;
}


