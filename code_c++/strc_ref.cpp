#include <iostream>
#include <string>

using namespace std;

struct free_throws
{
	string name;
	int made;
	int attemps;
	float percent;
};

void display(const free_throws &ft);
void set_pc(free_throws &ft);
free_throws & accumulate(free_throws & target,const free_throws &source);

int main(void)
{
	free_throws one = {"Ifslsa Branch", 13, 14};
	free_throws two = {"Andor Knott", 10, 16};
	free_throws three = {"Minnie Max", 7, 9};
	free_throws four = {"whily looper", 5, 9};
	free_throws five = {"long long" , 6, 14};
	free_throws team = {"throwgoods", 0, 0};
	
	free_throws dup;
	
	set_pc(one);
	display(one);
	accumulate(team, one);
	display(team);
	
	
	return 0;
	
}

void display(const free_throws & ft)
{
	cout << "Name: " << ft.name << '\n';
	cout << "Made: " << ft.made << '\n';
	cout << "Attemps: " << ft.attemps << '\n';
	cout << "Percent: " << ft.percent << '\n';
}

void set_pc(free_throws & ft)
{
	if(ft.attemps != 0)
	{
		ft.percent = 100.0f * float(ft.made)/float(ft.attemps);
	}
	else
	{
		ft.percent = 0;
	}
}

free_throws & accumulate(free_throws & target,const free_throws &source)
{
	target.attemps += source.attemps;
	target.made += source.made;
	set_pc(target);
	return target; 
}
