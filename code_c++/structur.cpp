
#include <iostream>
#include <string>
//c++的头文件不用带.h 

struct inflatable
{
	char name[20];//名字 
	float volume;//容量 
	double price; //售价 
};

int main()
{
	using namespace std;

	//inflatable str1;
	inflatable pal = 
	{
		"Audacions Arthur",
		3.12,
		32.99
	};
	//下面这个报错的原因？这不是初始化，千万不要觉得也是一种初始化方式 ，字符数组只能在初始化时赋值，不能直接将一个字符串赋值给字符数组
	//例如 char name[20] = “dog”；可以 但是name = “dog”；就是错的 
	//str1.name = 'dog';//为什么这个会报错 ?
	//str1.volume = 1;
	//str1.price = 3;//这个就不错 
	
	inflatable str1 = {"dog", 1, 3};
	inflatable palll = { };
	inflatable pallll {"cat",2,3};
    return 0;
}



