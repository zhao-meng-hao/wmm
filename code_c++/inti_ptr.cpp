
#include <iostream>
#include <string>
//c++的头文件不用带.h 


int main()
{
	using namespace std;
	
	int higgens = 5;
	//声明指针时初始化指针，被初始化的是指针，而不是它指向的值；
	//下面的语句是将pt的值设置为&higgens； 
	int *pt = &higgens;
	
	cout << "Value of higgens = " << higgens
		 << "; Address of higgens = " << &higgens << endl;
	cout << "Value of *pt = " << *pt
	 	 << "; Address of pt = " << pt <<endl;
    return 0;
}



