
#include <iostream>
#include <ctime>

const int ArSize = 20;

int main(void)
{
	using namespace std;

	char ch;
	int count = 0;
	cout << "enter characters; enter # to quit: \n";
	cin >> ch;
	while (ch != '#')
	{
		count++;
		cout << ch;
		cin >> ch;
	}
	cout << endl << count << " characters read\n";

    return 0;
}


