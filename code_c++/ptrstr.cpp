
#include <iostream>
#include <string>
#include <cstring>
//c++的头文件不用带.h 


int main()
{
	using namespace std;
	
	char animal[20] = "bear";
	const char * bird = "wren";
	char * ps;//这里未初始化，指针所指向的地址是未知的 
	
	cout << animal << " and ";
	cout << bird << "\n";
	
	cout << "Enter a kind of animal : ";
	cin >> animal;
	//cin >> ps;//因为上面ps未进行初始化，所以这里可能会出错 ，是不建议的 
	
	ps = animal ;//这是赋值地址
	cout << ps << "!\n";
	cout << "Before using strcpy():\n";
	cout << animal << " at " << (int *) animal << endl;
	cout << ps << " at " <<(int *) ps << endl;
	
	ps = new char [strlen(animal) + 1];
	strcpy(ps, animal);
	cout << "After using strcpy():\n";
	cout << animal << " at " << (int *)animal << endl;
	cout << ps << " at " << (int *) ps << endl;
	delete [] ps;
    return 0;
}

