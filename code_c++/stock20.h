#ifndef STOCK1O_H_
#define STOCK1O_H_

#include <string>

//本文件由 
class Stock
{
private:
    std::string company;//公司名称 
    long shares;//持有的股票数量 
    double share_val;//每股的价格 
    double total_val;//股票总价格 
    
    void set_tot() { total_val = shares * share_val; }

public:
	Stock();
	Stock(const std::string & co, long n = 0, double pr = 0.0);
	~Stock(); 
    //void acquire(const std::string &co, long n, double pr);
    void buy(long num, double price);
    void sell(long num, double price);
    void update(double price);
    void show() const;
    const Stock & topval(const Stock & s) const;
};

#endif
