
#include <iostream>
#include <vector>
#include <array> 

int main()
{

	using namespace std;
	double al[4] = {1.2, 3.2, 4.2, 5.2};
	vector <double> av(4);
	av[0] = 1.0/3.0;
	av[1] = 1.0/5.0;
	av[2] = 1.0/7.0;
	av[3] = 1.0/9.0;
	
	array<double, 4> aa = {3.14, 2.72, 1.64, 1.41};//这里我的编译器不支持，要求c++11版本 
	array<double, 4> aaa;
	aaa = aa;
	
	cout << "al[2]: " << al[2] << " at " << &al[2] << endl;
	cout << "av[2]: " << av[2] << " at " << &av[2] << endl;
	cout << "aa[2]: " << aa[2] << " at " << &aa[2] << endl;
	cout << "aaa[2]: " << aaa[2] << " at " << &aaa[2] << endl;
	
	al[-2] = 20.2;
	cout << "al[-2]: " << al[-2] << " at " << &al[-2] << endl;
	cout << "aa[2]: " << aa[2] << " at " << &aa[2] << endl;
	cout << "aaa[2]: " << aaa[2] << " at " << &aaa[2] << endl;
 	
    return 0;
}


