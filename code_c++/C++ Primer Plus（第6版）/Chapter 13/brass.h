// brass.h  -- bank account classes
// virtual 如果方法是通过引用或指针而不是对象调用的，它将确定使用哪一种方法。
#ifndef BRASS_H_
#define BRASS_H_
#include <string>
// Brass Account Class
class Brass
{
private:
    std::string fullName;	//客户姓名
    long acctNum;			//账号 
    double balance;			//当前余额 
public:
    Brass(const std::string & s = "Nullbody", long an = -1,
                double bal = 0.0);
    void Deposit(double amt);
    virtual void Withdraw(double amt);      //1
    double Balance() const;
    virtual void ViewAcct() const;			//2
    virtual ~Brass() {}  	// 虚析构函数，并不执行任何操作 
};

//Brass Plus Account Class
class BrassPlus : public Brass
{
private:
    double maxLoan;    //透支上限 
    double rate;       //透支贷款利率 
    double owesBank;   //当前的透支总额 
public:
    BrassPlus(const std::string & s = "Nullbody", long an = -1,
            double bal = 0.0, double ml = 500,
            double r = 0.11125);
    BrassPlus(const Brass & ba, double ml = 500, 
		                        double r = 0.11125);
    virtual void ViewAcct()const; 			//2
    virtual void Withdraw(double amt);		//1  两个类里面都有这两个函数，但是行为不同 
    void ResetMax(double m) { maxLoan = m; }
    void ResetRate(double r) { rate = r; };
    void ResetOwes() { owesBank = 0; }
};

#endif
