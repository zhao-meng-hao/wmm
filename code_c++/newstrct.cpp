
#include <iostream>
#include <string>
//c++的头文件不用带.h 
/*
着重看一下三个cin， 两种访问结构成员的方法
 （*ps）就是指结构本身 
*/
struct  str
{
	char name[20];
	float volume;
	double price;	
} ;
	
int main()
{
	using namespace std;
	
	str * ps = new str;
	cout << "Enter name of str item: ";//项目，条款 ，商品 
	cin.get(ps->name, 20);
	cout << "Enter volume in cubic feet: ";//体积，容积 
	cin >> (*ps).volume;
	cout << "Enter price: ( $ _ $ )";//价格 
	cin >> ps->price;
	cout << "Name: " << (*ps).name << endl;
	cout << "Volume: " << ps->volume << " cubic feet\n";
	cout << "price:( $ _ $ )" << ps->price << endl;
	delete ps; 
	
    return 0;
}

