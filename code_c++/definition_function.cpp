
#include <iostream>

/*
** 本程序是为了熟悉如何自定义函数并且调用；
	 
*/
void simon(int a);

int main(void)

{
	using namespace std;//涉及到命名空间，先不要管 
	simon(3);
	cout << "pick an integer:";//选择一个整数； 
	int count;
	cin >> count;
	simon(count);
	cout << "Deon!" <<endl; 

    return 0;
}

void simon(int a)
{
	using namespace std;
	cout << "simon says touch（摸摸） your toes(脚趾) " << a <<" times. " <<endl;
}

