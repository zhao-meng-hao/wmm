
#include <iostream>

const int NUMBER = 50;

int main(void)
{
	using namespace std;

	int x = 0;

	do
	{
		cin >> x;	
		if (x > NUMBER) 
			cout << "too big!" << endl;
		else if (x < NUMBER)
			cout << "too small!" << endl;
		else
			cout << "you are right!" << endl;
	}
	while(x != NUMBER);
    return 0;
}


