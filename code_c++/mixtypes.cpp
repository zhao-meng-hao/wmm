
#include <iostream>

struct years
{
	int year;	
} ;
	
int main()
{
	years s01,s02,s03;//定义三个years类型的结构变量 
	s01.year = 1998;//通过成员运算符访问成员 
	years *pa = &s02;//创建years类型的指针 
	pa->year = 1999;//通过箭头成员运算符访问成员 
	years trio[3];//创建years类型的数组 
	trio[0].year = 2003;//通过成员运算符访问元素的成员 
	std::cout << trio->year << std::endl;//数组名是地址，和指针类似 
	const years * arp[3] = {&s01, &s02, &s03};//创建指针数组 
	std::cout << arp[1]->year << std::endl;//数组的每一个成员都是一个指针 
	const years ** ppa = arp;//创建啊指向上面数组的指针 ，二级指针，ppa指向arp[0],而arp[0]也是一个指针 
	auto ppb = arp;//和上面是一样的 这里我的编译器编译不过 ，可能太老了 
	std::cout << (*ppa)->year << std::endl;//这就是arp的首个元素 &s01 
	std::cout << (*(ppb + 1))->year << std::endl;//这是第二个元素 &s02 
		
    return 0;
}


