
#include <iostream>
#include <string>
//c++的头文件不用带.h 


int main()
{
	using namespace std;
	char charr1[20];

	string str1;
	
	cout << "Enter a line of text : \n";
	cin.getline(charr1,20);//普通的c语言字符数组 读取一行 
	cout << "YOU entered: " << charr1 << endl;
	cout << "Enter another line of text: \n";
	getline(cin,str1);//c++ string对象  读取一行 
	cout << "YOU entered: " << str1 << endl;
	
    return 0;
}



