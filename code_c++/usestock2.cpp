#include <iostream>
#include "stock20.h"

int main(void)
{
	Stock stock[4] = 
	{
		Stock("NanoSmart", 12, 20.0),
		Stock("Boffo Objects", 200, 20.0),
		Stock("Monolithic Obelisks", 130, 3.25),
		Stock("Fleep Enterprises", 60, 6.5)
	};
	
	//using std::cout;
	std::cout << "Stock holding:\n";
	int st;
	for(st = 0;st < 4; st++){
		stock[st].show();//TODO
	}
	
	const Stock *top = &stock[0];
	for(st = 1;st < 4; st++){
		top = &top->topval(stock[st]);//TODO
	}
	std::cout<< "\nMost valuable holding:\n";
	top->show();
	
	return 0;
}
