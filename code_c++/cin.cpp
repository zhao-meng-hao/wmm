
#include <iostream>
//c++的头文件不用带.h 
/*
**这个主要熟悉cin的用法 ，相当于c语言的键盘输入 
*/
int main(void)

{
	using namespace std;//涉及到命名空间，先不要管 
	
	cout << "hello world!!" << endl; //输出hello world！；endl和\n的作用相同，换行 
	
	int carrots;
	
	cout << "How many carrots do you have?" <<endl;
	cin >> carrots;//键盘输入，流入到carrots
	cout << "here are two more.";
	carrots = carrots + 2;
	cout << "now you have " << carrots   << " carrots." <<endl;
    return 0;
}
