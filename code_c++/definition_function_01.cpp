
#include <iostream>

/*
** 本程序是为了熟悉如何自定义函数并且调用；
**定义一个有返回值的函数
**注：1英石=14磅
英国喜欢以英石（stone）为单位
美国喜欢以磅(pound)为单位 
	 
*/
int simon(int a);

int main(void)

{
	using namespace std;//涉及到命名空间，先不要管 

	cout << "一共多少英石: ";//
	int stone;
	int pound;
	cin >> stone;
	pound = simon(stone);
	cout << "您好，经过转换后是" <<pound <<"磅！" <<endl; 

    return 0;
}

int simon(int a)
{
	using namespace std;
	
	return a * 14 ;
}

