
#include <iostream>
//输入的字符是单个字符，不符合规则会进入死循环 

using namespace std;
void n_char(char c, int x); 

int main(void)
{
	int times;
	char ch;
	
	cout << "enter a character: ";
	cin >> ch;
	while(ch != 'q')
	{
		cout << "enter an integer: ";
		cin >> times;
		n_char(ch,times);
		cout << "\nenter an other character or press the q-key to quit: ";
		cin >> ch;
	}
	cout << "the value of times is " << times << ".\n";
	
	cout << "bye\n";
	
    return 0;
}

void n_char(char c, int x)
{
	while(x-- > 0)
		cout << c;
}


