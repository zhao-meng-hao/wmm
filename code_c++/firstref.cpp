#include <iostream>

int main(void)
{
	using namespace std;
	int rats = 101;
	int & rodents = rats;//创建引用变量 
	
	cout << "rats = " << rats;
	cout << ", rodents = " << rodents << endl;
	
	cout << "rats address = " << &rats << endl;
	cout << "rodents address = " << &rodents << endl;
	
	return 0;
	
}
