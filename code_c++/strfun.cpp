#include <iostream>
unsigned int c_in_str(const char *str, char ch);

int main(void)
{
	using namespace std;

	char mmm[15] = "minmaxmini";
	char *uuu = "ululuaru"; 
	unsigned int ms = c_in_str(mmm,'m');
	unsigned int us = c_in_str(uuu, 'u');
	cout << ms <<" m characters in " << mmm << endl;
	cout << us <<" u characters in " << uuu << endl;
    return 0;
}

unsigned int c_in_str(const char *str, char ch)
{
	unsigned int count = 0;
	
	while(*str)
	{
		if(*str == ch)
			count++;
		str++;	
	}
	return count;
}
