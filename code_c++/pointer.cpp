
#include <iostream>
//#include <string>
//c++的头文件不用带.h 


int main()
{
	using namespace std;

	int updates = 6;
	int *p_updates;//定义一个指针变量 
	p_updates = &updates;//把updates的地址赋值给变量p_updates 
	
	cout << "Values: updates = " << updates;
	cout << ", *p_updates = " << p_updates <<endl;
	
	*p_updates = 8;//解指针，来修改指向的值 
	cout << "Values: updates = " << updates << endl; 
	
	*p_updates = *p_updates + 1;
	cout << "Values: updates = " << updates << endl;
	
    return 0;
}



