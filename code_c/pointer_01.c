#include <stdio.h>

//指针
void pointer01()
{
	int a = 100;
	int b = 200;
	int z = 500;
	//int *p = a; 
	int *p;
	printf("%p\n",&a);//%p,指针的，打印地址	
	printf("%p\n",&b);	
	printf("%p\n",&z);	
	//printf("%p\n",&p);
	printf("%d\n",sizeof(p));//32位机器是4字节，64位机器是8字节 
	
}

int main(int argc,char *argv[])
{
	pointer01();
	return 0;
}

