#include<stdio.h>
/*
if……else分支语句
练习：
	判断一个数是否为奇数
	输出1~100之间的奇数 

*/ 
int main()
{

	int b = 0;
	
	while(b <= 100)
	{
		if(b % 2 == 1)
		{
			printf("%d ",b);
			b++;	
		}
	
	}
	return 0;
		
	 
}
