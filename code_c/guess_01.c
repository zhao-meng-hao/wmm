#include<stdio.h>
#include<stdlib.h>

/*
	猜数字 
	随机产生1-100的随机数 

*/ 

void menu()
{
	printf("******************************\n");
	printf("**********1.开始**************\n");
	printf("**********0.退出**************\n");
	printf("******************************\n");

}

void game()
{
	//生成随机数
	int re = rand()%100+1;//对100取余数是0--99，+1，范围就是1--100； 
	//rand函数返回一个0-32767之间的数字
	 
	int a = 0;
	
	//printf("%d\n",re);//用于打印产生的随机数 
	
	while(1)
	{
		printf("请输入您要猜的数字：\n");
		scanf("%d",&a); 
	
		if(a > re)
		{
			printf("您猜的数字大了！\n");
		}
		else if(a < re)
		{
			printf("您猜的数字小了！\n");
		}
		else
		{
			printf("恭喜你猜中了！\n");
			break;
		}	
	} 
	
}

 
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));//rand函数之前需要使用，
									//不然rand函数产生的数字是有规律的，不够随机 
	do
	{
		menu();	//打印菜单
		printf("请选择：");
		scanf("%d",&input); 
		
		switch(input)
		{
			case 1:
				printf("游戏开始\n"); 
				game();
				break;
			case 0:
			printf("退出游戏\n"); 
				break;
			default:
				printf("选择错误，重新选择:\n");
				break;
		}
	}while(input); 
	
	
	return 0;
		
	 
}

