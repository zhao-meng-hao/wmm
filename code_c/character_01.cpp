#include <stdio.h>
#include<string.h>

//转义字符的用处 
void character01()
{
	printf("d:\aabb\gtest.txt\n");// 转义字符\
	
	printf("dasda\ndann");// 换行符\n
	
	printf("%s\n","sadasd");
	
	printf("%s\n","\“");//"“" ，打印一个 “是错误的，要加转义字符\
	
	printf("%c\n",65);//A的ASCII码就是65
	
	//使用strlen函数需要添加头文件string.h；
	//\t是一个字符，\32是一个字符，
	//\后面跟三个数字是代表八进制数
	//\t是一个平行转义字符 
	printf("%d\n",strlen("c:\test\328\test.c")); 
	
}

int main(int argc,char *argv[])
{
	character01();
	
	return 0;
}
