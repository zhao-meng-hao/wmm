#include<stdio.h>

//数组 
//一个数组里面存放的一定是相同类型的数据 
void array01()
{
	int arr01[10] = {1,2,3,4,5,6,7,8,9,10};//数组初始化 
	char arr02[5] = {'a','b','c'};//不完全初始化，剩余的默认为0
	int i = 0;
	for(i =0;i < 10;i++){
		printf("%d\n",arr01[i]); 
	}
	
	printf("-------------\n");
	
	for(i =0;i < 5;i++){
		printf("%c\n",arr02[i]); 
	}
	 
	
}

int main(int argc, char *argv[])
{
	array01();
	return 0;
}
