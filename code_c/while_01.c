#include<stdio.h>
/*
while循环语句

*/ 
int main()
{
	int a = 0;
	while(a <= 10)
	{
		if(a == 6)
		{
			//在while循环里，break用于永久的终止循环
			
			//continue是跳过本次循环continue后面的代码，
			//直接去判断部分，看是否进行下一次循环 
			continue; 
			//break;
			printf("%d ",a);
			a++;	
		}	
	} 

	return 0;
		
	 
}

