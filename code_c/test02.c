#include <stdio.h>

//作用域和生命周期
//局部变量的作用域：就是变量所在的局部范围
//全局变量的作用域：整个工程 
//局部变量的生命周期： 进入局部范围生命开始，出局部范围生命结束 
//全局变量的生命周期： 程序的生命周期 

int a_var = 20;//全局变量 
 
void variable()
{
	int b_var = 50;
	printf("b_var = %d\n",b_var);
	printf("hello world!\n");
}

extern int a_var;

void variable01()
{
	//const修饰的变量--常变量，具有常属性，不能被改变 
	const int c_var = 100;
	
	int ch[10] = {0};
	
	//即使c_var被const修饰，但本质上还是一个变量 
//	int ch1[c_var] = {0}; //这里是不行的 
	int ch1[10] = {0};  
	
 } 
 
 #define MAX 10000 
 void variable03()
 {
 //	#define 定义的标识符常量,
// 不可以被改变 
// 	int MAX = 200;
	int d = MAX;
	printf("d = %d\n",d); 
  
  } 

//性别 
//enum关键字 
enum Sex
{
	//这种枚举类型的变量的未来可能取值 
	//枚举常量 ， 
	MALE = 3,//赋初值 男 
	FEMALE,//女 
	SECRET,//保密 
};

void variable04()
{
	//枚举常量不可以改变 
//	MALE = 5; 
	enum Sex s = MALE;
	printf("%d\n",MALE);
	printf("%d\n",FEMALE);
	printf("%d\n",SECRET);
}


int main(int argc, char *argv[])
{

	variable();
	printf("a_var = %d\n",a_var);
	variable03();
	variable04();
	
	return 0;	
}

