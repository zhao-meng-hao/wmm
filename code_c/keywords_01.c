#include<stdio.h>

//typedef：类型重定义 
//将unsigned int类型重定义为u_ int; 
typedef unsigned int u_int;

void keywords01()
{
	unsigned int num01 = 20;
	u_int num02 = 50;
}


//static --静态的
//可修饰局部变量、全局变量和函数
//static修饰局部变量，改变了局部变量的生命周期（本质上是改变了变量的存储类型） 
void keywords02()
{
	//局部变量开始与销毁
	//static修饰后，存储从栈区到了静态区，开始与销毁的周期变了 
	//int a = 0;
	static int a = 0;
	
	a++;
	printf("%d ",a);
 } 
 
 
//extern声明外部符号的
//在别的文件里定义的全局变量被static修饰后
//即使在本文件里声明后，也不能被使用；只能在自己的源文件里被使用； 
int main(int argv, char *argc[])
{
	int i = 0;
	while(i < 10)
	{
		keywords02();
		i++;
	}
	return 0;
} 
