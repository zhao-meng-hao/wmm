#include<stdio.h>

void operator01()
{
	int a = 20;
	//int b = ++a;//前置++，先++，后使用；
	int c = a++;//后置++，先自增，后++； 
	
	printf("%d\n",a);
	//printf("%d\n",b);
	printf("%d\n",c);
	
}

void operator02()
{
	int x = 0;
	int y = 50;
	int max = 0;
	
//	if(x > y)
//	{
//		max = x;
//	}	
//	else
//	{
//		max = y;	
//	}
//	printf("%d\n",max);
	
	//以上运算可以使用三目操作符
	max = (x > y) ? x : y;
	printf("%d\n",max);
	
}

int main(int argc,char *argv[])
{
	operator01();
	operator02();
	
	return 0;
	
}
