#include<stdio.h>

/*
	有序数组 
	二分查找 
*/ 
int main(int argc, char *argv[])
{

	int ch[] = {1,2,3,4,5,6,7,8,9,10};
	int k = 7;//要查找的数字
	int lang = sizeof(ch)/sizeof(ch[0]);//数组元素个数 
	
	int left = 0;//左下标 
	int right = lang - 1;//右下标 
	int m = 0;
	
	//while(ch[m] != k )
	while(left <= right )
	{
			int m = (left + right) / 2;//查找中间
			if (ch[m] < k)
			{
				left = m + 1;
			}
			else if(ch[m] > k)
			{
				right = m - 1; 
			}
			else
			{
		
			printf("找到了! ch[%d] = %d\n",m,ch[m]); 
				break;
			}			
	}
	if(left > right)
	{
		printf("找不到了！"); 
	}
	

	 
	return 0;
}
