#include<stdio.h>
#include<string.h>

void test01()
{
	//打印hello world 
	printf("hello world\n");
}

void test02()	
{
	char ch = 'a'; 
	int b = 100;
	printf("ch = %c\n",ch);//打印ch的值 
	printf("b = %d\n",b);//打印b 
	printf("ch所占字节 = %d\n",sizeof(ch));//打印ch所占字节 
	printf("b所占字节 = %d\n",sizeof(b));//打印b所占字节 
}

void test03()
{
	int age = 20;//年龄 
	float weight = 62.5;//体重 
	
//	age++;
	age += 1;
	weight += 5;
//	age = age + 1;
//	weight = weight + 5;
	
	printf("年龄 = %d\n",age);//打印年龄 
	printf("体重 = %f\n",weight);//打印体重 float输出用%f，double输出用%lf 
 } 

void test04()
{
	int c = 10;
	printf("c = %d\n",c);
 } 
//当局部变量和全局变量名字冲突时，局部优先 

//int c = 100;

void test05()
{
	int d = 0;
	int e = 0;
	int f = 0;
	
	printf("请输入2个数:\n");
	scanf("%d%d",&d, &e);//键盘输入2个数据 
	getchar();//使用scanf函数之后最好加一句getchar（），读回车 
	f = d + e;//求和 
	printf("f = %d\n",f);//打印求和结果 
}

/*
如何求10个整数的最大值 
*/
void test06()
{
	int ch[10] = {0};
	int i = 0;
	
	//键盘输入十个数据分别存放在数组各个元素内 
	for(i = 0;i < 10;i++) 
	{
		printf("请输入一个数据：\n");
		scanf("%d",&ch[i]);	
		getchar();	
	}
	int max = ch[0];
	int x = 1;
	
	//比较数据大小，把最大值存放在max变量里 
	for(x = 1;x < 10;x++)
	{
		if(ch[x] > max)
		{
			max = ch[x];
		}
	}
	
	//打印键盘输入的十个数据 
	for(i = 0;i < 10;i++)
	{
		printf("%d\n",ch[i]);
	}
	//printf("ch[%d] = &d\n",i,&ch[i]);
	printf("最大值为%d\n",max);
 } 

/*
如何求十个数的和并算出平均值 
*/ 
void test07()
{
	int ch1[10] = {0};
	int i = 0;
	for(i = 0;i < 10;i++)
	{
		//键盘输入十个数据，存到ch数组里 
		printf("请输入一个数：\n");
		scanf("%d",&ch1[i]);
		getchar();
	}
	
	int x = 0;
	int sum = 0;
	
	//求和，打印和 和平均值 
	for(x = 0;x <10;x++)
	{
		sum += ch1[x];
	  }  
	  printf("sum = %d\n",sum);
	  printf("平均值为%d\n",sum/10);
 } 

int main(int argc, char *argv[])
{
	test07();
	
	return 0;
 } 
