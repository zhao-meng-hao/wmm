#include "stdio.h"
#include "stdlib.h"

#define CardNumber 13 
typedef int ElemType;

typedef struct Node
{
	ElemType data;
	struct Node *next;
 } sqlist, *LinkList;
 
 
 LinkList CreateLinkList()
 {
 	LinkList head=NULL;
 	LinkList s,r;
 	int i;
 	
 	r=head;
 	for(i=1;i<=CardNumber;i++)
 	{
 		s=(LinkList)malloc(sizeof(sqlist));
 		s->data=0;
 		
 		if(head==NULL)
 			head=s;
 		else
 		r->next=s;
 		
 	    r=s;
		 
	 }
	 r->next =head;
	 
	 return head;
 }

//发牌顺序计算 
 void Magician (LinkList head)
 {
 	LinkList p;
 	int j;
 	int CountNumber=2;
 	
 	p=head;
 	p->data=1;//第一张牌放1 
 	
 	while(1)
 	{
 		for(j=0;j<CountNumber;j++)
 		{
 			p=p->next;
 			if(p->data !=0)//该位置有牌的话，则下一个位置 
 			{
 				p->next;
 				j--;
			 }
		 }
		 if(p->data==0)
		 {
		 	p->data=CountNumber;
		 	CountNumber++;
		 	
		 	if(CountNumber==14)
		 		break;
		  } 
	 }
}

//销毁工作
void DestoryList(LinkList *list)
{
	LinkList ptr = *list;
	LinkList buff[CardNumber];
	int i=0;
	
	while(i<CardNumber)
	{
		buff[i++]=ptr;
		ptr=ptr->next;
		
	}
	
	for(i=0;i<CardNumber;i++)
	free(buff[i]);
	
	*list=0;
 } 

int main()
{
	LinkList p;
	int i;
	p=CreateLinkList();
	Magician(p);
	
	printf("按如下顺序排列：\n");
	
	for(i=0;i<CardNumber;i++)
	{
		printf("黑桃%d ",p->data);
		p=p->next;
		
	}
	
	DestoryList(&p);
	
	return 0;
}



 
 
