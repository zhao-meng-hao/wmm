#include<stdio.h>

//while循环语句
 
int main(int argc, char *argv[])
{
	char word[20] = {};
	printf("请输入密码：>");//假如密码是123456 fdsfdsf ，中间有个空格 
	scanf("%s",word); //数组名就是一个地址，所以不需要加 & ；字符数组和整形数组的区别 
	printf("请确认密码：（Y/N）>");
	
/*
解释：scanf输入密码：123456\n,缓存区存储123456\n，123456存到word数组里；
	缓存区还有一个\n，getchar（）就会读取到\n， 起到清理缓存区的作用；
	//如果不加一个getchar（），后面就会读取到\n，无论输入什么都是密码错误 
*/	 
	//清理缓存区 
	getchar();//处理‘\n ’ 
	
	//清理缓存区的多个字符 
	int temp = 0;//是由于getchar（）返回的是int型，ASCII码值 
	while((temp = getchar()) != '\n')
	{
		;
	} 

	
	int ch = '0';
	ch = getchar();
	if(ch == 'Y')
	{
		printf("密码正确！"); 
	}else
	{
		printf("密码错误！");
	}
	
	return 0;
}
