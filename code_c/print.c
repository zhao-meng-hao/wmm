#include <stdio.h>

//输入一串字符串，以#为结束
//输出结果为输入字符串的倒序
//比如输入abc#，打印输出cba 
void print()
{
	char a;
	scanf("%c",&a);
	if(a != '#') print();
	if(a != '#') printf("%c",a);
} 

int main()
{
	print();
	return 0;
}
