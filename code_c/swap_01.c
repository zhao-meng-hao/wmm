#include<stdio.h>

/*
	函数的初步了解
	写一个函数：交换2个整形变量的值 
*/ 

//错误的，地址的原因 
//无法交换两个数据的值 
void swap(int x, int y)
{
	int temp = 0;
	temp = x;
	x = y;
	y = temp; 
}

void swap01(int* x, int* y)
{
	int temp = 0;
	temp = *x;
	*x = *y;
	*y = temp;
}

//p是指针变量，用来存放地址
//“ * ”是解引用操作符，
//*p就是打开p号盒子，取出里面的数据。
int main(int argc, char *argv[])
{
	int a = 10;
	int b = 20;
	
	printf("%d %d\n",a,b); 
	//调用函数 
	swap01(&a,&b);
	printf("%d %d\n",a,b); 
	
	
	return 0;
}
