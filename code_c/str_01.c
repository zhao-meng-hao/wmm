#include<stdio.h>
#include<string.h>

void str01()
{
	char arr[] = "hello";//隐含了一个\0; 
	printf("%d\n",sizeof(arr));	
	printf("----------------\n");
	char arr01[] = "abcd";
	char arr02[] = {'a','b','c','d'};
	printf("%s\n",arr01);//打印arr01中保存的字符串 
	printf("%s\n",arr02);//打印arr02中保存的字符串
	printf("%d\n",sizeof(arr01));//打印arr01所占字节 
	printf("%d\n",sizeof(arr02));//打印arr02所占字节 
	printf("----------------\n");
	printf("%d\n",strlen(arr01));//打印arr01字符串长度 
	printf("%d\n",strlen(arr02));//打印arr02字符串长度
	
}

int main(int argc,char *argv[])
{
	str01();
	
	return 0;
}
