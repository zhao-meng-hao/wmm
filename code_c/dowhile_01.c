#include<stdio.h>

/*do……while循环语句

格式:do 
		循环语句；
	while（表达式）； 
 
*/ 
int main(int argc, char *argv[])
{
	int i = 0;
	do
	{
		printf("%d ",i);
		i++;	
	}
	while(i < 10); 
	
	return 0;
}
