#include <stdio.h>

/*参数：
	n：移动圆盘数
	x，y，z：三个柱子
*/
//将n个盘子从x借助y移动到z	 
void move(int n,char x,char y,char z)
{
	static int i=1;
	
	if(1==n)
	{
		printf("%c->%c\n",x,z);
		i++;
		printf("移动了%d次\n",i);
    }
    else
    {
    	move(n-1,x,z,y);     //将n-1个盘子从x借助z移动到y上 
		printf("%c->%c\n",x,z);//将第n个盘子从x移动到z上 
		move(n-1,y,x,z);     //将n-1个盘子从y借助x移动到z上
		i+=2; 
	}

} 

int main()
{
	int n;
	printf("请输入汉诺塔的层数：");
	scanf("%d",&n); 
	printf("移动的步骤如下:\r\n");
	move(n,'x','y','z');
	
	return 0; 
 } 
