#include <stdio.h>

int main(int argc, char *argv[])
{
	int *s;//定义一个指针变量s 
	int b;//定义一个变量 
	b=20;//给变量赋值 
//	int temp;
    s=&b;//把变量b的内存地址赋值给变量s
    //temp=*s;

//	int a,b,c;
//	printf("a b c=\n");
//	scanf("%d %d %d",&a,&b,&c);
//	
//	printf("a=%d b=%d c=%d\n",a,b,c);
    printf("0x%x\n",s);//打印指针s的地址 
    printf("0x%x\n",&b);//打印变量b的地址
	 
	printf("%d\n",*s);//通过指针变量访问变量的值 
	
	//通过指针变量修改变量的值 
	*s=22;
	printf("%d\n",b);
	printf("%d\n",*s);
//	printf("%d\n",sizeof(s));
	return 0;
}
