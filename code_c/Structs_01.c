#include<stdio.h>

//结构体
struct stu
{
	char name[20];//姓名 
	int age;//年龄 
	char gender[2];//性别 
	
}zhuzhuxia = {"猪猪侠", 5 , "男"}; //初始化 
void structs01()
{
	//初始化方式 
	struct stu wangwu;
	wangwu.age = 30;
	
	//初始化 
	struct stu lisi = {"李四",20,"男"};
	
	printf("1:%s %d %s\n",lisi.name,lisi.age,lisi.gender);//结构体打印的格式	
	printf("%s %d %s\n",zhuzhuxia.name,zhuzhuxia.age,zhuzhuxia.gender);	
	
	struct stu *p = &lisi;
	printf("2: %s %d %s\n",(*p).name,(*p).age,(*p).gender);
	
	printf("3: %s %d %s\n",p->name,p->age,p->gender);
}
 
int main()
{
	structs01();
	return 0;
}
