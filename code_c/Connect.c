//假设A,B为非空循环链表的尾指针

//伪代码,LinkList是定义的一个结构体，保存结点的数据域和指针域
 
LinkList Connect(LinkList A,LinkList B)
{
	LinkList P = A->next;//保存A表的头结点位置 
	A->next = B->next->next;//B表的开始结点链接到A表尾 
	
	free(B->next);//释放B表的头结点 
	
	B->next = P; //
	return B;//返回新的循环链表的尾指针 
}

