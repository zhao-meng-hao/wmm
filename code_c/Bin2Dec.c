#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define STACK_INIT_SIZE 20
#define STACKINCREMENT 10

//发现，如果需要对结构体内的数据进行修改，参数传递*s（指针）
//不需要修改，直接传s； 
typedef char ElemType;

typedef struct
{
	ElemType *base;
	ElemType *top;
	int stackSize;
}sqStack;

//初始化栈 
void Init(sqStack *s)
{
	s->base = (ElemType *) malloc(STACK_INIT_SIZE * sizeof(ElemType));
	if(!s->base)
		exit(0);//退出
		
	s->top = s->base; 
	s->stackSize = STACK_INIT_SIZE;
}
//压栈 
void Push(sqStack *s,ElemType e)
{
	if(s->top - s->base >= s->stackSize)
	{
		s->base = (ElemType *) realloc(s->base,(s->stackSize + STACKINCREMENT) * sizeof(ElemType));
		if(!s->base)
			exit(0);
	}
	*(s->top) = e;
	s->top++;
}

//出栈
void Pop(sqStack *s,ElemType *e)
{
	if(s->top == s->base)
		return;
	
	*e = *--(s->top); 
}

//求当前栈容量
int StackLem(sqStack s)
{
	return(s.top - s.base);	//结构体是 . 指针是 -> 
} 

int main(void)
{
	ElemType c;
	sqStack s;
	int len,i,sum=0;
	Init(&s);
	
	printf("请输入二进制数，输入#符号表示结束！\n");
	scanf("%c",&c);
	while(c != '#')
	{
		Push(&s,c);
		scanf("%c",&c);
	}
	getchar();//清除回车 
	
	len= StackLem(s); 
	printf("当前容量为%d\n",len);
	
	for(i=0;i<len;i++)
	{
		Pop(&s,&c);
		sum=sum+(c-48)*pow(2,i);
	}
	printf("转化我十进制数是：%d\n",sum);
	
	return 0;
}

