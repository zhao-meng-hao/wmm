#include<stdio.h>
#include<math.h>
/*
	打印100——200之间的素数
	就是质数
	只能被1和它本身整除 
	
	m = a*b
	a和b 中至少有一个数字是 <= m开平方的
	
	aqrt--开平方函数 
	需要math.h
	
	变量：
		i用于存放100 -- 200
		y用于存放2 -- i-1 
		x用于存放有多少个素数 
*/ 



int main(int argc,char *argv[])
{
	int x = 0;
	int i;
	for(i = 100; i <= 200; i++)
	{
		int y;
		//2--i-1之间的数字试除，看能不能被整除 
		//for(y = 2; y <= sqrt(i); y++)//dev c++运行不出来 
		for(y = 2; y < i; y++)
		{
			if(i % y == 0)
			{
				break;
			}
		}
		if(i == y)
		{
			printf("%d是素数\n",i);
			x++;	
		}
		
	} 
	printf("%d\n",x);
	return 0;
}
