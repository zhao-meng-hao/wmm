#include<stdio.h>

/*
	函数的初步了解
	练习： 
		写一个函数：prime_number判断一个整数是不是素数 
		写一个函数：opinion_leapYear,判断一年是不是闰年 
		写一个函数：binary_search,实现一个整形有序数组的二分查找 
		写一个函数：num_add,每调用一次这个函数，就会将num的值增加1 //改变了实参的值，址传参 
*/ 

int prime_number(int x)
{
	int y = 0;
	for(y = 2; y < x; y++)
			{
				if(x % y == 0)
				{
					return 0;
					break;
				}
			}
			return 1;
}

int opinion_leapYear(int x) 
{
	if(x % 4 == 0 && (x % 100 != 0 || x % 400 == 0))
	{
		return 1;
	}
	else
	{
		return 0;
	} 
}


int binary_search(int x[],int y,int z)
{
	
	int left = 0;
	int right = z - 1;
	int m = 0;
	
	while(left <= right)
	{
		int m = (left + right) / 2;//查找中间
		if (x[m] < y)
		{
			left = m + 1;
		}
		else if(x[m] > y)
		{
			right = m - 1; 
		}
		else
		{
			return m;
		}		
	}
	return -1;//找不到 
}

int num_add(int*p)
{
	(*p)++;
}

int main(int argc, char *argv[])
{
	//int a = 0;
	//int ch[] = {1,2,3,4,5,6,7,8,9,10};
	//int k = 7; 
	//int z = 0;
	//z = sizeof(ch)/sizeof(ch[0]);
	//假如要找的数是7
	//找到了就返回找到的位置的下标
	//找不到返回-1 
	//数组ch传参，实际传递的不是数组的本身
	//仅仅传过去了数组首元素的地址 
//	int ret = binary_search(ch,k,z);
//	if(-1 != ret)
////	{
//		printf("找到了！ch[%d]",ret);
//	}
//	else
//	{
//		printf("找不到了！"); 
//	}
	 
	//printf("请输入一个数:");
	//scanf("%d",&a);
	
	//if(prime_number(a) == 1)
	//if((opinion_leapYear(a)) == 1)
	//{
	
		//printf("%d是素数！\n",a);
		//printf("%d是闰年！\n",a);
		
	//}

	int count = 0;
	num_add(&count);
	printf("%d\n",count);
	num_add(&count);
	printf("%d\n",count);
	num_add(&count);
	printf("%d\n",count);
	return 0;
}
