#include<stdio.h>
/*
switch分支语句

*/ 
int main()
{
	int a = 0;
	int b = 1;
	switch(a)
	{
		case 0:
			b++;
		case 1:
			a++;
		case 2:
			switch(a)
			{//switch嵌套 
				case 0:
					a++;
				case 1:
					b++;
					a++;
					//break跳出最里层循环 
					break; 
			}
		case 4:
			b++;
			break;
		default:
			break;	
	}
	printf("b = %d, a = %d\n",b,a);

	return 0;
		
	 
}
